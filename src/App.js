import * as React from "react";
import List from "./List";
import Footer from "./Footer";
import './App.css';

class App extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            data: [],
            show: 'all',
            showClearButton: 'none',
            selectAll: true,
            complete: false,
            showFooter: 'none',
            showSelectAllButton: 'none'
        };
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.handleClick = this.handleClick.bind(this);
        this.filterItem = this.filterItem.bind(this);
        this.clearCompleted = this.clearCompleted.bind(this);
        this.selectAll = this.selectAll.bind(this);
        this.showAll = this.showAll.bind(this);
        this.completeItem = this.completeItem.bind(this);
        this.showClearButton = this.showClearButton.bind(this);
        this.changeStatus = this.changeStatus.bind(this);
        this.showFooter = this.showFooter.bind(this);
        this.showSelectAllButton = this.showSelectAllButton.bind(this);
    }

    handleKeyDown(event) {
        if (event.keyCode === 13) {
            const savedData = this.state.data;
            const newItem = {
                item: event.target.value,
                status: 'active'
            };
            savedData.push(newItem);
            this.setState({
                data: savedData
            });
            event.target.value = '';
        }
        this.showFooter();
        this.showSelectAllButton();
    };

    handleClick(e) {
        let value = e.target.value;
        let data = this.state.data;

        const item = e.target.parentElement
            .getElementsByClassName('label')[0].innerHTML;
        const itemIndex = this.state.data
            .indexOf(this.state.data.filter(ele => ele.item === item)[0]);
        const status = this.state.data[itemIndex].status;

        if (value === 'complete') {
            this.changeStatus(e, data, itemIndex, status);
        } else {
            data.splice(itemIndex, 1);
        }
        this.setState(
            {
                data: data
            }
        );
        this.showClearButton();
        this.showFooter();
        this.showSelectAllButton();
    }

    changeStatus(e, data, itemIndex, status) {
        let newStatus;
        if (status === 'completed') {
            newStatus = 'active';
            e.target.parentElement.getElementsByClassName('label')[0].classList.remove('completed');

        } else {
            newStatus = 'completed';
            e.target.parentElement.getElementsByClassName('label')[0].classList.add('completed');

        }
        data[itemIndex].status = newStatus;
    }

    showFooter() {
        const itemCount = this.state.data.length;
        if (itemCount > 0) {
            this.setState({
                showFooter: 'block'
            })
        } else {
            this.setState({
                showFooter: 'none'
            })
        }
    }

    showClearButton() {
        const completedItemCount = this.state.data.filter(ele => ele.status === 'completed').length;
        if (completedItemCount > 0) {
            this.setState(
                {
                    showClearButton: 'block'
                }
            )
        } else {
            this.setState(
                {
                    showClearButton: 'none'
                }
            )
        }
    }

    showSelectAllButton() {
        const itemCount = this.state.data.length;
        if (itemCount > 0) {
            this.setState({
                showSelectAllButton: 'inline'
            })
        } else {
            this.setState({
                showSelectAllButton: 'none'
            })
        }
    }

    completeItem(newData, completedItemIndex, completedItem, status) {
        newData[completedItemIndex] = {
            item: completedItem,
            status: status
        }
    }

    filterItem(e) {
        let value = e.target.value;
        const buttons = e.target.parentElement.getElementsByClassName('filters');
        switch (value) {
            case 'active':
                e.target.classList.add('selected');
                buttons.item(0).classList.remove('selected');
                buttons.item(2).classList.remove('selected');
                break;
            case 'completed':
                e.target.classList.add('selected');
                buttons.item(0).classList.remove('selected');
                buttons.item(1).classList.remove('selected');
                break;
            case 'all':
                e.target.classList.add('selected');
                buttons.item(1).classList.remove('selected');
                buttons.item(2).classList.remove('selected');
                break;
            case 'clear':
                this.clearCompleted();
                this.showSelectAllButton();
                this.showFooter();
                this.setState(
                    {
                        showClearButton: 'none'
                    }
                );
                break;
        }
        this.setState({
            show: value
        });
    }

    selectAll(e) {
        const selectAll = this.state.selectAll;
        this.setState({
            selectAll: !selectAll
        });
        if (this.state.selectAll) {
            this.showAll('completed', 'inline');

        } else {
            this.showAll('active', 'none')
        }
        let allCheckbox = e.target.parentElement.getElementsByClassName('toggle');
        let allCheckBoxArray = Array.from(allCheckbox);
        for (let i = 0; i < allCheckBoxArray.length; i++) {
            allCheckBoxArray[i].checked = selectAll;
        }
    }

    showAll(status, showClearButton) {

        const data = this.state.data;
        const allCompletedData = data.map(ele => {
            return {
                item: ele.item,
                status: status
            }
        });
        this.setState({
            data: allCompletedData
        });
        this.setState(
            {
                showClearButton: showClearButton
            }
        );
    }

    clearCompleted() {
        const activeItems = this.state.data.filter(ele => ele.status === 'active');
        this.setState(
            {
                data: activeItems
            }
        );
    }

    render() {
        return (
            <div className='App'>
                <header className='header'>
                    <h1>todos</h1>
                    <input type='text' className='new-todo' placeholder='What needs to be done?'
                           onKeyDown={this.handleKeyDown}/>
                </header>
                <section className='main'>
                    <input type='checkbox' className='toggle-all' id='toggle-all' onClick={this.selectAll}/>
                    <label htmlFor='toggle-all' style={{display: this.state.showSelectAllButton}}/>
                    <List data={this.state.data} show={this.state.show} onClick={this.handleClick}/>
                </section>
                <Footer data={this.state.data}
                        showClearButton={this.state.showClearButton}
                        onClick={this.filterItem}
                        showFooter={this.state.showFooter}/>
            </div>
        )
    }

}

export default App;

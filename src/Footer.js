import * as React from "react";
import './Footer.css';

class Footer extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            all: true,
            active: false,
            completed: false,

        }
    }

    render() {
        const itemCount = this.props.data.filter(item => item.status === 'active').length;
        return (
            <footer className='footer' style={{display: this.props.showFooter}}>
                <span>{itemCount} item left</span>
                <span className='filter'>
                <button className='filters selected' onClick={this.props.onClick} value='all'>All</button>
                <button className='filters' onClick={this.props.onClick} value='active'>Active</button>
                <button className='filters' onClick={this.props.onClick} value='completed'>Completed</button>

                </span>
                <button className='clear-completed' onClick={this.props.onClick} value='clear'
                        style={{display: this.props.showClearButton}}>
                    Clear Completed
                </button>
            </footer>
        )
    }
}

export default Footer;

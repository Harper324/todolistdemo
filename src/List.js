import * as React from "react";
import './List.css';

class List extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            completed: false
        };
    }

    render() {
        const showStatus = this.props.show;
        let lists;
        switch (showStatus) {
            case 'active':
                lists = this.props.data.filter(e => e.status === 'active');
                break;
            case 'completed':
                lists = this.props.data.filter(e => e.status === 'completed');
                break;
            default:
                lists = this.props.data;
        }
        const showItem = lists.map(e =>
            <li key={e.item.toString()}>
                <input className='toggle' type='checkbox' onClick={this.props.onClick} value='complete'/>
                <label className={e.status === 'completed' ? 'label completed' : 'label'}>{e.item}</label>
                <button className='button material-icons' onClick={this.props.onClick} value='destroy'>
                    clear
                </button>
            </li>);

        return (
            <ul className='todo-list'>
                {showItem}
            </ul>
        )
    }
}

export default List;
